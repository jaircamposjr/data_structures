/**
 * 
 * @author jairc
 *
 * creates a treenode for the mytree contrainer.
 * contains the data, a left and right treenode and a count
 * for repeating data.
 */

public class TreeNode{
	String data;
	TreeNode left,right;
   int count = 1;
}