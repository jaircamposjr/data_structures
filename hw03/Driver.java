/**
 * This is the driver for the MyTree program. It initializes the bibleTree and imports the files.
 * This will also print the stats for this application. 
 *
 * @author jairc
 *
 */
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.File;
public class Driver {
	public static MyTree bibleTree = new MyTree();
   /**
    *main method. Initiates file and begins its traversal.
    *
   **/
   public static void main(String args[]) throws FileNotFoundException {
      //new file import
      File file = new File("../data/biblewords.txt");
      Scanner f = new Scanner(file);
      //begins the application
      bibleTree.begin(f);
      //traverses the bibleTree
      bibleTree.traverse(bibleTree.root);
      //sorts so you can get top 20 used bible words
      bibleTree.sortTop20();
      printTimers();
   }
   /**
    *print the timers that were monitored during application
    *
    *@return void
   **/
   public static void printTimers(){
      System.out.println();
      System.out.println("Time spent inserting: "+bibleTree.inserting.toString());
      System.out.println("Time spent sorting: "+bibleTree.sorting.toString());
      System.out.println("Time spent total: "+bibleTree.total.toString());
   }
}