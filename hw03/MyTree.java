/**
 *The MyTree Class is the container. it is manipulated by the driver
 *uses TreeNode.java
 *
 *@author Jair Campos
**/
import java.io.File;
import java.util.Scanner;

public class MyTree{
      //root of the tree
      public static TreeNode root = null;
      //pointer is the main pointer in traversing the tree
      public static TreeNode pointer= root; 
      //follower follows pointer incase we need to go back
      public static TreeNode follower = pointer;
      public static TreeNode[] top20 = new TreeNode[15000];
      public Timer inserting = new Timer();
      public Timer sorting = new Timer();
      public Timer total = new Timer();
      public int count;
      
      /**
       *Begin method starts the application. substitute for "main".
       *this will control the insert
       *
       *@return void
      **/
      public void begin (Scanner f) {
         total.start();
         inserting.start();
         //if the file still has data we want to create a new node
         while (f.hasNextLine()){
            String bibleWord = f.nextLine();
            TreeNode result = search(bibleWord);
            //if the word was not found insert that node
            if(result == null){
               insert(bibleWord);
            //if the word has already been inserted we just increment that count
            }else if(result.data.equals(bibleWord)){
               result.count++;
            }
         }
         inserting.stop();
      }

      /**
       *insert method will insert data into nodes
       *
       *@return void
      **/
      public void insert (String newdata){
   		TreeNode n;
         
         n = new TreeNode();
         n.data = newdata;
         n.left = null;
         n.right = null;
         //System.out.println(newdata);
         if (root == null){
            	root = n;
         }else{
            pointer=root;
            follower=pointer;
            while(pointer!=null){
               follower=pointer;
               if (newdata.compareToIgnoreCase(pointer.data) > 0){
                  pointer=pointer.right;
               }else{
                  pointer=pointer.left;
               }
            }
            if(newdata.compareToIgnoreCase(follower.data) > 0){
               follower.right = n;
            }else{
               follower.left = n;
            }
	      }
      }
      /**
       *delete method. deletes a value in the tree.
       *
       *@return void
      **/
      public void delete(String val){
         TreeNode x,y,z;
         
         if (root == null){
            return;
         }
         
         x=root;
         y=null;
         while (x!=null && x.data != val){
            y = x;
            if(val.compareToIgnoreCase(x.data) == -1){
               x = x.left;
            }else{
               x = x.right;
            }
         }
         if(x==null){
            return;
         }
         if (x.left != null && x.right != null){
            z = x;
            y = x;
            x = x.right;
            
            while (x.left != null){
               y = x;
               x = x.left;
            }
            z.data = x.data;
            if (x.left != null){
               z = x.left;
            }else{
               z = x.right;
            }
            if (y == null) {
               root=z;
            }
            else if (y.left == x){
               y.left = z;
            }else{
               y.right = z;
            }
         }
      } 
      /**
       *traverse method. traverses the tree
       *
       *@return void
      **/
   public void traverse(TreeNode node){
      if(node != null){
         insertTop20(node);
         traverse (node.left);
         traverse (node.right);
      }
   }
   /**
    *insertTop20 inserts the top 20 values in an array
    *
    *@return void
   **/
   public void insertTop20(TreeNode node){
      top20[count] = node;
      count++;
   }
   /**
    *sortTop20 sorts the top 20 values in an array
    *
    *@return void
   **/
   public void sortTop20(){
      sorting.start();
      TreeNode temp;
      for(int i = 0; i<top20.length; i++){
         if(top20[i] != null){               
            for(int j = 1; j<top20.length-i; j++){
               if(top20[j] != null){
                  if(top20[j-1].count < top20[j].count){
                     temp = top20[j-1];
                     top20[j-1] = top20[j];
                     top20[j] = temp;
                  }
               }
            }
         }
      }
      sorting.stop();
      display();
   }
   /**
    *display prints the top 20 values from the top20 array
    *
    *@return void
   **/
   public void display(){
      for(int i=0;i<20;i++){
         TreeNode temp = top20[i];
         System.out.println(temp.data+": "+temp.count);
      }
      total.stop();
   }
   /**
    *searches for a string value and returns the node that its in.
    *
    *@return a TreeNode
   **/
   public TreeNode search(String searchVal){
   	TreeNode pointer= root;
      if (pointer == null){
         return pointer;
      }
      while(pointer != null){
     	   if (searchVal.compareTo(pointer.data) < 0){
     	   	pointer= pointer.left;
         }else if (searchVal.compareTo(pointer.data) > 0){
         	pointer= pointer.right;
         }else{
            return pointer;
         }
      }
      return pointer;
   }
   /**
    *getRoot gets the root of the tree
    *
    *@return TreeNode root
   **/
   public TreeNode getRoot(){
      return root;
   }
}
