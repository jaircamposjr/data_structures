/**
 * @author = Jair Campos
 *
 *uses a sort called MergeSort to sort an imported file.
**/
import java.util.*;
import java.io.*;

public class MergeSort{
   //this will be my main array
   Contributor[] contributors; 
   //this is a temp array for later
   Contributor[] contLeft;
   //temp array for later 
   Contributor[] contRight; 
   //timer
   Timer timer = new Timer(); 
   // 2 scanners to read in the file
   Scanner counter,parser; 
   // msc ints
   int i,amount = 0; 
   // this will say how many Contributors are in the file
   int count = 0; 
   //msc strings
   String first,last,loadTime,sortTime; 
   
   /**
    * load method to load in all the contributors into an array
    *
    * @param f file string path
   **/
   public void load(String f)throws FileNotFoundException{
      timer.start();
      //first array to count how many cont. are in the file
      counter = new Scanner(new File(f));
      while (counter.hasNextLine()){
         count++;
         counter.nextLine();
      }
      //second array that will load in the data
      parser = new Scanner(new File(f));
      contributors = new Contributor[count];
      
      for(i=0; i<count; i++){
         first = parser.next();
         last = parser.next();
         amount = parser.nextInt();

         contributors[i] = new Contributor(first, last, amount);
         
      }
      timer.stop();
      loadTime = timer.toString();
      timer.reset();
      timer.start();
      sort(contributors,0,count-1);
   }
   /**
    *partition to help with quicksort
    *
   **/
   public void merge(Contributor[] contributors, int left, int middle, int right) { 
        int one = middle - left + 1;
        int two = right - middle;
        
        //temp arrays left and right
        contLeft = new Contributor[one];
        contRight = new Contributor[two];
          
        for(i=0; i<one; i++){ 
            contLeft[i] = contributors[left+i];
        }
        for(int j=0; j<two; j++){
            contRight[j] = contributors[middle + 1 + j];
        }
        
        i = 0;
        int j = 0;
  
        int k = left; 
        while (i < one && j < two){ 
            if (contLeft[i].compareTo(contRight[j])>=0){ 
                contributors[k] = contLeft[i]; 
                i++; 
            }else{ 
                contributors[k] = contRight[j]; 
                j++; 
            } 
            k++; 
        } 
  
        while (i < one){ 
            contributors[k] = contLeft[i]; 
            i++; 
            k++; 
        } 
  
        while (j < two){ 
            contributors[k] = contRight[j]; 
            j++; 
            k++; 
        } 
    } 
  
  
    /* 
     * main quicksort method sort
     *  
     * @param contributors[] , low, high
     *
    **/
    void sort(Contributor contributors[], int left, int right){ 
         timer.reset();
         timer.start();
        if (left<right){ 
            //index with partition
            int middle = (left+right)/2; 
  
            //recursively sort elements
            sort(contributors, left, middle); 
            sort(contributors, middle+1, right); 
            
            merge(contributors, left, middle, right);
        } 
        timer.stop();
        sortTime = timer.toString();
    }
   
   /**
    * print the stats
    *
    *@return void
   **/
   public void print() throws FileNotFoundException{
         System.out.println("Load Time: "+loadTime);
         System.out.println("Sort Time: "+sortTime);
         System.out.println("-------------------");
   }
}