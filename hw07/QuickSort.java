/**
 * @author = Jair Campos
 *
 *uses QuickSort algorithm to sort a file.
**/
import java.util.*;
import java.io.*;

public class QuickSort{
   //this will be my main array
   Contributor[] contributors; 
   //timer
   Timer timer = new Timer(); 
   // 2 scanners to read in the file
   Scanner counter,parser; 
   // msc ints
   int i,amount = 0; 
   // this will say how many Contributors are in the file
   int count = 0; 
   //msc strings
   String first,last,loadTime,sortTime; 
   
   /**
    * load method to load in all the contributors into an array
    *
    * @param f file string path
    * @return void
   **/
   public void load(String f)throws FileNotFoundException{
      timer.start();
      //first array to count how many cont. are in the file
      counter = new Scanner(new File(f));
      while (counter.hasNextLine()){
         count++;
         counter.nextLine();
      }
      //second array that will load in the data
      parser = new Scanner(new File(f));
      contributors = new Contributor[count];
      
      for(i=0; i<count; i++){
         first = parser.next();
         last = parser.next();
         amount = parser.nextInt();

         contributors[i] = new Contributor(first, last, amount);
         
      }
      timer.stop();
      loadTime = timer.toString();
      timer.reset();
      timer.start();
      sort(contributors,0,count-1);
   }
   /**
    *partition to help with quicksort
    *
    *@return int that is used for the index
   **/
   public int partition(Contributor[] contributors, int low, int high) { 
        Contributor pivot = contributors[high];  
        i = (low-1); // get the index of smallest cont amount 
        for (int j=low; j<high; j++){ 
            //if j is greater than high we want to switch
            if (contributors[j].compareTo(pivot)>=0){ 
                i++; 
                Contributor temp = contributors[i]; 
                contributors[i] = contributors[j]; 
                contributors[j] = temp; 
            } 
        }
        
        Contributor temp = contributors[i+1]; 
        contributors[i+1] = contributors[high]; 
        contributors[high] = temp;
        return i+1; 
    } 
  
  
    /* 
     * main quicksort method sort
     *  
     * @param contributors[] , low, high
     *
     *@return void
    **/
    public void sort(Contributor contributors[], int low, int high){ 
        if (low<high){ 
            //index with partition
            int index = partition(contributors, low, high); 
  
            //recursively sort elements
            sort(contributors, low, index-1); 
            sort(contributors, index+1, high); 
        } 
    }
   
   /**
    * print the stats
    *
    *@return void
   **/
   public void print() throws FileNotFoundException{
   timer.stop();
   sortTime = timer.toString();
         System.out.println("Load Time: "+loadTime);
         System.out.println("Sort Time: "+sortTime);
         System.out.println("-------------------");
   }
}