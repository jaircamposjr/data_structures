/**
 * @author = Jair Campos
 *
 * uses a selection sort algorithm to sort a method.
**/
import java.util.*;
import java.io.*;

public class SelectionSort{
   //this will be my main array
   Contributor[] contributors;
   //timer 
   Timer timer = new Timer(); 
   // 2 scanners to read in the file
   Scanner counter,parser; 
   // msc ints
   int i,amount = 0; 
   // this will say how many Contributors are in the file
   int count = 0; 
   //msc strings
   String first,last,loadTime,sortTime; 
   
   /**
    * load method to load in all the contributors into an array
    *
    * @param f file string path
   **/
   public void load(String f)throws FileNotFoundException{
      timer.start();
      //first array to count how many cont. are in the file
      counter = new Scanner(new File(f));
      while (counter.hasNextLine()){
         count++;
         counter.nextLine();
      }
      //second array that will load in the data
      parser = new Scanner(new File(f));
      contributors = new Contributor[count];
      
      for(i=0; i<count; i++){
         first = parser.next();
         last = parser.next();
         amount = parser.nextInt();

         contributors[i] = new Contributor(first, last, amount);
         
      }
      timer.stop();
      loadTime = timer.toString();      
   }
   /**
    *sort method. Selection sort
    *
    *@return void
   **/
   public void sort(){
   timer.reset();
   timer.start();
   Contributor temp = new Contributor("","",0);
      for (i = 0; i<contributors.length-1; i++){
         for (int j = i+1; j<contributors.length; j++){ 
            //if j is greater than i they need to be switched
            if (contributors[j].compareTo(contributors[i])>0){
               temp = contributors[i];
               contributors[i]=contributors[j];
               contributors[j]=temp;
            }
         } 
      } 
   timer.stop();
   sortTime = timer.toString();
   }
   
   /**
    * print the stats
    *
    *@return void
   **/
   public void print() throws FileNotFoundException{
         System.out.println("Load Time: "+loadTime);
         System.out.println("Sort Time: "+sortTime);
         System.out.println("-------------------");
   }
}