import java.io.*;
import java.util.*;

/**
 *@author jair
 * 
 * SelectionSort, Built-in Sort, Merge Sort, the quicksort was from a previous version, but I am keeping it as extra.
 *
**/
public class Driver
{
	public static void main(String [] args) throws FileNotFoundException{

      SelectionSort ss = new SelectionSort();
      ss.load(args[0]);
      ss.sort();
      ss.print();
      
      
      BultIn bi = new BultIn();
      
      bi.load(args[0]);
      bi.sort();
      bi.print();

      //i couldnt figure out 2 more versions of quicksort so i did merge sort. this is extra
      QuickSort qs = new QuickSort();
      
      qs.load(args[0]);
      qs.print();
      

      MergeSort merger = new MergeSort();
      
      merger.load(args[0]);
      merger.print();
	}
}
