/**
* Implements a red-black tree for T objects.
*
* @author  Terry Sergeant
* @version for Data Structures homework
* @see RBTreeNode<T>
*/

import java.util.*;
import java.io.*;

public class RBTree<T extends Comparable<T>>
{
	protected RBTreeNode<T> NIL;  // sentinel node
	protected RBTreeNode<T> root; // marks root of tree
	protected int n;           // number of nodes in tree
	protected int height;      // height of tree
	protected int bheight;     // black height of tree

	// NOTE: height and bheight are NOT kept by insert and delete
   //       You must explcitly call calcStats to update values.


	/**
	* Creates new tree and defines NIL.
	*/
	public RBTree()
	{
		NIL= new RBTreeNode<T>(null,NIL);
		NIL.left= NIL;
		NIL.right= NIL;
		root= NIL;
		n= height= bheight= 0;
	}


	/**
	* Show tree stats.
	*/
	public String toString()
	{
		return "size("+n+"), height("+height+"), black height("+bheight+")";
	}


	/**
	* Getter for the NIL node.
	* @return reference to NIL node
	*/
	public RBTreeNode<T> NIL()
	{
		return NIL;
	}


	/**
	* Traverses tree to calculate n, height, and bheight.
	*/
	public void calcStats()
	{
		n= height= bheight= 0;
		doCalc(root,1,1);
	}


	/**
	* Does the "dirty work" for calcStats() by recursively obtaining stats.
	*
	* @param x current node being evaluated
	* @param h height/depth of current node
	* @param bh black height of current node
	*/
	protected void doCalc(RBTreeNode<T> x, int h, int bh)
	{
		if (x != NIL) {
			n++;
			if (h > height)	 height= h;
			if (bh > bheight) bheight= bh;
			if (x.right.color=='B')
				doCalc(x.right,h+1,bh+1);
			else
				doCalc(x.right,h+1,bh);
			if (x.left.color=='B')
				doCalc(x.left,h+1,bh+1);
			else
				doCalc(x.left,h+1,bh);
		}
	}


	/**
	* Performs left-rotate operation on node x.
	*/
	public void leftRotate(RBTreeNode<T> x)
	{
      RBTreeNode<T> node = x.right;
      //if x is the root
      if(x.parent == NIL){
         x.parent = node;
         x.right = node.left;
         x.right.left = x;
         
         if(x.right != NIL){
            x.right.parent = x;
         }
         x.parent.parent = NIL;
         root = x.parent;
      }else{
          if(x.parent.right == x){
            x.parent.right = node;
          }else{
            x.parent.left = node;
          }
          x.right.parent = x.parent;
          x.right.left = x;
          x.parent = node;
          x.right = x.right.left;
          if(x.right != NIL){
            x.right.parent = x;
          }
      }
	}


	/**
	* Performs a right-rotate operation on node x.
	*/
	public void rightRotate(RBTreeNode<T> x)
	{
      RBTreeNode<T> node = x.left;
      //if x is the root
      if(x.parent == NIL){
         x.parent = node;
         x.left.right = x;         
         x.left = node.right;
         
         if(x.left != NIL){
            x.left.parent = x;
         }
         root = x.parent;
         x.parent.parent = NIL;
      }else{
          if(x.parent.left == x){
            x.parent.left = node;
          }else{
            x.parent.right = node;
          }
          x.left.parent = x.parent;
          x.left.right = x;
          x.parent = node;         
          x.left = x.left.right;
          if(x.left != NIL){
            x.right.parent = x;
          }
      }
  }


	/**
	* Getter for height.
	*
	* @return height of tree
	*
	* <p>NOTE: Must call calcStats() before this is valid.</p>
	*/
	public int height()
	{
		return height;
	}


	/**
	* Getter for black height.
	*
	* @return black height of tree
	*
	* <p>NOTE: Must call calcStats() before this is valid.</p>
	*/
	public int bheight()
	{
		return bheight;
	}


	/**
	* Getter for number of nodes in the tree.
	*
	* @return black height of tree
	*
	* <p>NOTE: Must call calcStats() before this is valid.</p>
	*/
	public int size() { return n; }


	/**
	* Searches for node with specified key.
	*
	* @param data data to find
	* @return reference to requested node; NIL if not found.
	*/
	public RBTreeNode<T> find(T data)
	{
		RBTreeNode<T> x= root;
		int result;

		while (x != NIL) {
			result= data.compareTo(x.data);
			if (result==0)
				return x;
			if (result < 0)
				x= x.left;
			else
				x= x.right;
		}
		return NIL;
	}


	/**
	* Displays contents of tree via an inorder traversal.
	*/
	public void inorder()
	{
		inorder(root);
	}


	/**
	* Helper function for inorder() that recursively performs inorder traversal.
	*
	* @param p current node being looked at during inorder traversal
	*/
	public void inorder(RBTreeNode<T> p)
	{
		if (p==NIL) return;
		inorder(p.left);
		System.out.println(p.data);
		inorder(p.right);
	}


	/**
	* Inserts node into red-black tree.
	*
	* @param data reference to data object of node being inserted
	* @return reference to newly inserted node
	*/
	public RBTreeNode<T> insert(T data)
	{
		RBTreeNode<T> prev= findInsertLocation(data);
		RBTreeNode<T> newNode= new RBTreeNode<T>(data,'R',NIL,prev);
		insertFixup(newNode,prev);
		n++;
		return newNode;
	}


	/**
	* Searches tree for location of insert.
	*
	* @param data data to be inserted
	* @return reference to parent-to-be of new node
	*/
	protected RBTreeNode<T> findInsertLocation(T data)
	{
		RBTreeNode<T> prev, x;
		int result;

		prev= NIL;
		x= root;
		while (x != NIL)
		{
			prev= x;
			result= data.compareTo(x.data);
			if (result < 0)
				x= x.left;
			else
				x= x.right;
		}
		return prev;
	}


	/**
	* Restores red-black tree properties after an insert.
	*
	* @param newNode reference to node that was just inserted
	* @param prev reference to parent of node that was just inserted
	*/
	protected void insertFixup(RBTreeNode<T> newNode, RBTreeNode<T> prev)
	{
		RBTreeNode<T> x,y;
		int result;

		if (prev == NIL)		// if first node
			root= newNode;
		else {
			result= newNode.data.compareTo(prev.data);
			if (result < 0)
				prev.left= newNode;
			else
				prev.right= newNode;
		}

		/* fix up tree if necessary */
		x= newNode;
		while ((x!=root) && (x.parent.color=='R')) {
			if (x.parent == x.parent.parent.left) {      /* cases 1-3 */
				y= x.parent.parent.right;
				if (y.color == 'R') {                     /* case 1 */
					x.parent.color= 'B';
					y.color= 'B';
					x.parent.parent.color= 'R';
					x= x.parent.parent;
				}
				else {
					if (x == x.parent.right) {             /* case 2 */
						x= x.parent;
						leftRotate(x);
					}
					x.parent.color= 'B';                   /* case 3 */
					x.parent.parent.color= 'R';
					rightRotate(x.parent.parent);
				}
			}
			else {                                       /* symmetric cases */
				y= x.parent.parent.left;
				if (y.color == 'R') {                     /* case 1' */
					x.parent.color= 'B';
					y.color= 'B';
					x.parent.parent.color= 'R';
					x= x.parent.parent;
				}
				else {
					if (x == x.parent.left) {              /* case 2' */
						x= x.parent;
						rightRotate(x);
					}
					x.parent.color= 'B';                   /* case 3' */
					x.parent.parent.color= 'R';
					leftRotate(x.parent.parent);
				}
			}
		}
		root.color= 'B';
	}


	/**
	* Removes node from red-black tree.
	*
	* @param data data to be removed
	* @return true if key is in tree, false otherwise
	*/
	public boolean delete(T data)
	{
		RBTreeNode<T> x,y,z= find(data);   // z is node to be removed
		if (z == NIL) return false;

		if (z.right != NIL && z.left != NIL) { // y is node to be spliced out
			y= z.right;
			while (y.left != NIL)               // which is succ(z) if z has 2 children
				y= y.left;
		}
		else
			y= z;

		if (y.left !=NIL)                      // x is child of y (possibly NIL)
			x= y.left;
		else
			x= y.right;
		x.parent= y.parent;

		if (y == root)                         // splice out y
			root= x;
		else if (y==y.parent.left)
			y.parent.left= x;
		else
			y.parent.right= x;

		if (y != z) {        // if node spliced out different than node
			z.data= y.data;   // to be removed, copy appropriate info.
		}

		if (y.color=='B')
			deleteFixup(x);

		n--;
		return true;
	}



	/**
	* Restores red-black tree properties after a node is deleted.
	*
	* @param x child of node that was spliced out of the tree by BST delete
	*/
	protected void deleteFixup(RBTreeNode<T> x)
	{
		RBTreeNode<T> w;

		while (x != root && x.color=='B') {
			if (x.parent.left==x) {              // cases 1-4 (regular)
				w= x.parent.right;
				if (w.color=='R') {               // case 1
					w.color='B';
					x.parent.color='R';
					leftRotate(x.parent);
					w= x.parent.right;
				}

				if (w.left.color=='B' && w.right.color=='B') {  // case 2
					w.color= 'R';
					x= x.parent;
				}
				else {
					if (w.right.color=='B') {                    // case 3
						w.left.color= 'B';
						w.color= 'R';
						rightRotate(w);
						w= x.parent.right;
					}

					w.color= x.parent.color;                     // case 4
					x.parent.color= 'B';
					w.right.color= 'B';
					leftRotate(x.parent);
					x= root;
				}
			}
			else {                                            // symmetric cases 1-4
				w= x.parent.left;
				if (w.color=='R') {                            // case 1'
					w.color='B';
					x.parent.color='R';
					rightRotate(x.parent);
					w= x.parent.left;
				}

				if (w.right.color=='B' && w.left.color=='B') { // case 2'
					w.color= 'R';
					x= x.parent;
				}
				else {
					if (w.left.color=='B') {                    // case 3'
						w.right.color= 'B';
						w.color= 'R';
						leftRotate(w);
						w= x.parent.left;
					}

					w.color= x.parent.color;                    // case 4'
					x.parent.color= 'B';
					w.left.color= 'B';
					rightRotate(x.parent);
					x= root;
				}
			}
		}
		x.color= 'B';
	}
}
