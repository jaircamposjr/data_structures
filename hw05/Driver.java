/**
* Driver for red-black tree implementation of bible words.
*
* @author  T.Sergeant
* @version for Data Structures Homework
*
*/

import java.util.*;
import java.io.*;

public class Driver
{
   public static RBTree bibleTree = new RBTree();
   public static BibleWord bibleWord;
         
   public static void main(String args[]) throws FileNotFoundException {
      File file = new File("../data/biblewords.txt");
      Scanner f = new Scanner(file);
      
      Timer timer = new Timer();
      timer.start();
	   while (f.hasNextLine()){
         String word = f.nextLine();
         bibleWord = new BibleWord(word);
         bibleTree.insert(bibleWord);
      }
      timer.stop();
      System.out.println("Time spent inserting: "+timer.toString());
      
      BibleWord find = new BibleWord("Lord");
      System.out.println(bibleTree.find(find).toString());
   }
}

