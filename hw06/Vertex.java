/**
 * Represents a vertex in a shortest-path graph.
 *
 * @author  T.Sergeant
 * @version for Data Structures
 */

class Vertex implements Comparable<Vertex>
{
	public int id;				// 0 to n-1 identifying which vertex this is
	public int dist;			// best distance discovered so far between this vertex and the starting vertex
	public Vertex prev;	 // reference to the vertex that discovered the best distance so far to this vertex

	/**
	 * Constructor identifies the node id and initial distance.
	 *
	 * @param id the vertex's id number
	 * @param dist the initial distance of this vertex from the source vertex
	 */
	public Vertex(int id, int dist)
	{
		this.id= id;
		this.dist= dist;
		prev= null;
	}


	/**
	 * The vertex with the greatest distance is considered greater.
	 *
	 * @param other the vertex we are comparing this to
	 *
	 * <p>It is helpful to implement Comparable so we can use the built-in
	 * PriorityQueue class for efficient selection of the next vertex to work
	 * with.</p>
	 */
	@Override
	public int compareTo(Vertex other)
	{
		return this.dist - other.dist;
	}
}


