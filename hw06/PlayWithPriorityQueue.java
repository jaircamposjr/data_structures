/**
 * Code to play with the built-in PriorityQueue (which will be helpful for
 * creating an reasonably efficient solution to Dijkstra's Single Source
 * Shortest Path algorithm).
 *
 * @author	T.Sergeant
 * @version for Data Structures
 *
*/

import java.util.PriorityQueue;

public class PlayWithPriorityQueue
{

	public static void main(String [] args)
	{
		PriorityQueue<Vertex> pq;
		Vertex curr;

		pq= new PriorityQueue<Vertex>();

		pq.add(new Vertex(0,0));
		pq.add(new Vertex(1,100));
		pq.add(new Vertex(2,200));
		pq.add(new Vertex(3,50));
		pq.add(new Vertex(4,10));
		pq.add(new Vertex(5,150));
		pq.add(new Vertex(6,30));

		while (pq.size()>0) {
			curr= pq.poll();
			System.out.println("Just removed "+curr.id+" whose distance was "+curr.dist);
		}
	}
}
