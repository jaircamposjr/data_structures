public class MyHeap{
   private double[] a;
   private int n;
   
   public MyHeap(){
      a = new double[10];
      n = 0;
   
   }
   
   public int left(int index){
      return 2*index+1;
   }
   
   public int right(int index){
      return 2*index+2;
   }
   
   public int parent(int index){
      return (index-1)/2;
   }

   public void insert(double val){
      int temp = n;
      a[temp] = val;
      if(temp > 0 && a[temp] > a[parent(temp)]){
         swap(temp,parent(temp));
         temp = parent(temp);
      }
      n++;
   }
      
   public void swap(int n, int parent){
      while(a[n]>a[parent]){
         double temp;
         temp = a[n];
         a[n]=a[parent];
         a[parent]=temp;
      }
   }
   
   public double delete(){
      double val = a[0];
      a[0] = a[n-1];
      n--;
      heapify(0);
      return(val);
   }
   
   public void heapify(int x){      
      if(left(x) != n && a[x]<a[left(x)]){
         swap(x,left(x));
         heapify(left(x));
      }         
      if(right(x) != n && a[x]<a[right(x)]){
         swap(x,right(x));
         heapify(right(x));
      }
   }
}