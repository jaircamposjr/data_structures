public class Driver{
   public static void main(String[] args){
      MyHeap heap = new MyHeap();
      
      heap.insert(0);
      heap.insert(1);
      heap.insert(2);
      heap.insert(3);
      heap.insert(4);
      heap.insert(5);
      heap.insert(6);
      heap.insert(7);
      heap.insert(8);
      heap.insert(11);
      
      System.out.println(heap.delete());
      System.out.println(heap.delete());
      System.out.println(heap.delete());
      System.out.println(heap.delete());
      System.out.println(heap.delete());


   }
}